﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geschenke
{
    /*
     * Geschenk-Exception
     */

    class GeschenkException : GeschenkAppException
    {
        public GeschenkException(string meldung) : base(meldung) {}
    }

    /*
     * Geschenk
     */
    abstract class Geschenk
    {
        protected Geschenk(int id)
        {
            m_id = id;
            Bezeichner = "Noname";
        }

        protected Geschenk(int id, string bezeichner)
        {
            m_id = id;
            Bezeichner = bezeichner;
        }

        // ---

        private int m_id;
        public int ID
        {
            get { return m_id; }
        }

        private string m_bezeichner;
        private string Bezeichner
        {
            get
            {
                return m_bezeichner;
            }
            set
            {
                string trimmedValue = value.Trim();
                if (trimmedValue.Length == 0)
                {
                    throw (new GeschenkException("Der Bezeichner darf nicht leer sein."));
                }
                m_bezeichner = trimmedValue;
            }
        }

        // ---

        public virtual string getBeschreibung()
        {
            return string.Format("[{0}] {1} ", m_id.ToString(), Bezeichner);
        }

        public virtual void veraendere()
        {
            Console.Write("Bezeichner: ");
            Bezeichner = Console.ReadLine();
        }

        public abstract bool isSelbstgemacht();
    }

    /*
     * SelbstGeschenk
     */
    class SelbstGeschenk : Geschenk
    {

        public SelbstGeschenk(int id) : base(id)
        {
            Schwierigkeitsgrad = 1;
        }

        public SelbstGeschenk(int id, string bezeichner, int schw)
            : base(id, bezeichner)
        {
            Schwierigkeitsgrad = schw;
        }

        // ---

        protected int m_schwierigkeitsgrad;
        private int Schwierigkeitsgrad
        {
            get
            {
                return m_schwierigkeitsgrad;
            }
            set
            {
                if (value < 1 || value > 3)
                {
                    throw (new GeschenkException("Schwierigkeitsgrad muss zwischen 1-3 sein"));
                }
                m_schwierigkeitsgrad = value;
            }
        }

        // ---

        public override bool isSelbstgemacht()
        {
            return true;
        }

        public override void veraendere()
        {
            base.veraendere();

            Console.Write("Schwierigkeitsgrad (1-3): ");
            Schwierigkeitsgrad = Utils.getIntInput();
        }

        public override string getBeschreibung()
        {
            return base.getBeschreibung()
                + String.Format(" (Schwierigkeitsgrad:{0})", Schwierigkeitsgrad);
        }
    }


    /*
     * KaufGeschenk
     */
    class KaufGeschenk : Geschenk
    {

        public KaufGeschenk(int id) : base(id)
        {
            Hersteller = "Noname";
        }

        public KaufGeschenk(int id, string bezeichner, string hersteller)
            : base(id, bezeichner)
        {
            Hersteller = hersteller;
        }

        // ---

        private string m_hersteller;
        private string Hersteller
        {
            get
            {
                return m_hersteller;
            }
            set
            {
                string trimmedValue = value.Trim();
                if (trimmedValue.Length == 0)
                {
                    throw (new GeschenkException("Der Hersteller darf nicht leer sein."));
                }
                m_hersteller = trimmedValue;
            }
        }

        // ---

        public override bool isSelbstgemacht()
        {
            return false;
        }

        public override void veraendere()
        {
            base.veraendere();

            Console.Write("Hersteller: ");
            Hersteller = Console.ReadLine();
        }

        public override string getBeschreibung()
        {
            return base.getBeschreibung()
                + String.Format(" (Hersteller:{0})", Hersteller);
        }
    }
}
