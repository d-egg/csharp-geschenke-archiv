﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geschenke
{
    /*
     * Geschenkladen
     *
     * Manage eine Sammlung von Geschenken
     */

    class GeschenkLadenException : GeschenkAppException
    {
        public GeschenkLadenException(string meldung) : base(meldung) {}
    }

    class GeschenkLaden
    {
        protected List<Geschenk> m_geschenke;

        public GeschenkLaden()
        {
            m_geschenke = new List<Geschenk>();
        }

        // ---

        protected Geschenk chooseGeschenkById()
        {
            int chosenID = Utils.getIntInput();

            Geschenk g = m_geschenke.Find(ge => ge.ID == chosenID);
            if (g == default(Geschenk))
            {
                throw (new GeschenkLadenException("Kein Geschenk mit der ID: " + chosenID));
            }
            return g;
        }

        protected int getNextFreeID()
        {
            if (m_geschenke.Count == 0)
            {
                return 0;
            }

            // Extract all IDs into an array and sort it
            int[] allIds = new int[m_geschenke.Count];
            for (int i = 0; i < m_geschenke.Count; i++)
            {
                allIds[i] = m_geschenke[i].ID;
            }
            Array.Sort(allIds);

            // find the first free ID in the sorted IDs
            for (int j = 0; j < allIds.Length; j++)
            {
                if (j != allIds[j])
                {
                    return j;
                }
            }
            // no holes, return the next;
            return allIds.Length;
        }

        protected void movePosition(bool wantUp, Geschenk g)
        {
            int idxChoice = m_geschenke.FindIndex(ge => ge.ID == g.ID);
            if (idxChoice < 0)
            {
                throw (new ApplicationException("(invalid call) Kein Geschenk mit der ID: " + idxChoice));
            }

            int lowerIndex = wantUp ? idxChoice - 1 : idxChoice;
            if (lowerIndex < 0 || lowerIndex > (m_geschenke.Count - 2))
            {
                throw (new GeschenkLadenException("Das Element ist schon in einer extremen Position."));
            }

            int rangeCount = 2;
            m_geschenke.Reverse(lowerIndex, rangeCount);
        }

        // ---

        public void moveGeschenkUp()
        {
            Console.Write("Wähle das aufwärtstendierende Geschenk (id): ");
            Geschenk g = chooseGeschenkById();
            movePosition(true, g);
        }

        public void moveGeschenkDown()
        {
            Console.Write("Wähle das abwärtstendierende Geschenk (id): ");
            Geschenk g = chooseGeschenkById();
            movePosition(false, g);
        }

        public void sortGeschenkeByID()
        {
            m_geschenke.Sort((x, y) => x.ID < y.ID ? -1 : 1);
        }

        public void addStandardGeschenke()
        {
            m_geschenke.Add(new KaufGeschenk(getNextFreeID(), "Baum", "Wald"));
            m_geschenke.Add(new KaufGeschenk(getNextFreeID(), "Dünger", "Tier"));

            m_geschenke.Add(new SelbstGeschenk(getNextFreeID(), "Kette", 1));
            m_geschenke.Add(new SelbstGeschenk(getNextFreeID(), "Origamischwan", 3));
            m_geschenke.Add(new SelbstGeschenk(getNextFreeID(), "Schneeball", 1));

            m_geschenke.Add(new KaufGeschenk(getNextFreeID(), "Wasser", "Wolke"));
            m_geschenke.Add(new KaufGeschenk(getNextFreeID(), "Wind", "Windrad"));
        }

        public void anzeigen()
        {
            foreach (Geschenk g in m_geschenke)
            {
                Console.WriteLine(g.getBeschreibung());
            }
        }

        public void modGeschenk()
        {
            Console.Write("Wähle das zu verändernde Geschenk (id): ");
            Geschenk g = chooseGeschenkById();
            g.veraendere();
        }

        public void addNewGeschenk()
        {
            Geschenk g = null;
            Console.Write("Wähle den Typ des Geschenkes (k|s): ");
            string typWahl = Console.ReadLine();
            if (typWahl == "k")
            {
                g = new KaufGeschenk(getNextFreeID());
            }
            else if (typWahl == "s")
            {
                g = new SelbstGeschenk(getNextFreeID());
            }
            else
            {
                throw (new GeschenkLadenException("Ungültige Wahl."));
            }
            g.veraendere();
            m_geschenke.Add(g);
        }

        public void removeAllGeschenke()
        {
            m_geschenke.Clear();
        }

        public void removeOneGeschenk()
        {
            Console.Write("Wähle das zu entfernende Geschenk (id): ");
            Geschenk g = chooseGeschenkById();
            m_geschenke.RemoveAll(ge => ge.ID == g.ID);
        }

        public void removeAllSelbstGeschenke()
        {
            m_geschenke.RemoveAll(ge => ge.isSelbstgemacht());
        }

        public void removeAllKaufGeschenke()
        {
            m_geschenke.RemoveAll(ge => !ge.isSelbstgemacht());
        }
    }
}
