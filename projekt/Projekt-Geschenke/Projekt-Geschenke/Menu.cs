﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geschenke
{
    /*
     * Menu
     *
     * Menu-Loop: Zeige alle Geschenke, zeige das Menu, wähle eine Operation
     */

    class MenuException : GeschenkAppException
    {
        public MenuException(string meldung) : base(meldung) { }
    }

    class GeschenkMenu
    {
        GeschenkLaden m_geschenkLaden;
        MenuEntry[] m_menuEntries;

        protected delegate void MenuOperation();

        struct MenuEntry
        {
            public MenuEntry(string desc, MenuOperation op)
            {
                description = desc;
                operation = op;
            }
            public string description;
            public MenuOperation operation;
        }

        // ---

        public GeschenkMenu()
        {
            m_geschenkLaden = new GeschenkLaden();
            m_geschenkLaden.addStandardGeschenke();

            m_menuEntries = new MenuEntry[] {
                new MenuEntry("anzeigen",
                        new MenuOperation(m_geschenkLaden.anzeigen)),
                new MenuEntry("füllen",
                        new MenuOperation(m_geschenkLaden.addStandardGeschenke)),
                new MenuEntry("hinzufügen",
                        new MenuOperation(m_geschenkLaden.addNewGeschenk)),
                new MenuEntry("ändern",
                        new MenuOperation(m_geschenkLaden.modGeschenk)),
                new MenuEntry("löschen",
                        new MenuOperation(m_geschenkLaden.removeOneGeschenk)),
                new MenuEntry("löschen (alle)",
                        new MenuOperation(m_geschenkLaden.removeAllGeschenke)),
                new MenuEntry("löschen (SelbstGeschenke)",
                        new MenuOperation(m_geschenkLaden.removeAllSelbstGeschenke)),
                new MenuEntry("löschen (KaufGeschenke)",
                        new MenuOperation(m_geschenkLaden.removeAllKaufGeschenke)),
                new MenuEntry("hoch",
                        new MenuOperation(m_geschenkLaden.moveGeschenkUp)),
                new MenuEntry("runter",
                        new MenuOperation(m_geschenkLaden.moveGeschenkDown)),
                new MenuEntry("sortieren",
                        new MenuOperation(m_geschenkLaden.sortGeschenkeByID)),
            };
        }

        // ---

        private void outputErrorMessage(string msg)
        {
            Console.WriteLine("\nFEHLER. " + msg);
        }

        private void outputExceptionError(Exception e)
        {
            outputErrorMessage(e.Message);
        }

        private void displayGeschenke()
        {
            Console.WriteLine("\n[==== Geschenke ====]");
            m_geschenkLaden.anzeigen();
        }

        private void displayMenu()
        {
            Console.WriteLine("\n[==== Menu ====]");
            for (int i = 0; i < m_menuEntries.Length; i++)
            {
                MenuEntry me = m_menuEntries[i];
                Console.WriteLine(String.Format("[{0}] {1}", i, me.description));
            }
        }

        private void interactWithMenu()
        {
            Console.Write("\nWähle eine Operation: ");
            int chosenMenuID = Utils.getIntInput();
            if (chosenMenuID < 0 || chosenMenuID >= m_menuEntries.Length)
            {
                throw (new MenuException("Ungültige Wahl"));
            }
            m_menuEntries[chosenMenuID].operation();
        }

        public void menuLoop()
        {
            string errorMessage = null;
            while (true)
            {
                try
                {
                    displayGeschenke();
                    displayMenu();
                    if (errorMessage != null)
                    {
                        outputErrorMessage(errorMessage);
                    }
                    interactWithMenu();
                    errorMessage = null;
                }
                catch (GeschenkAppException e)
                {
                    errorMessage = e.Message;
                }
            }
        }
    }
}
