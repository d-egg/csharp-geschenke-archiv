﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_Geschenke
{
    /*
     * Application-weite Exception class
     */
    class GeschenkAppException : ApplicationException
    {
        public GeschenkAppException(string meldung) : base(meldung) { }
    }

    /*
     * Hilfmittel die an unterschiedlichen Orten im Programm benötigt werden.
     */
    static class Utils
    {
        static public int getIntInput()
        {
            try
            {
                return Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException e)
            {
                throw (new GeschenkAppException(e.Message));
            }
        }
    }
}
